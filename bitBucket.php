<?php
/*******************************************************************************
* Bitbucket WebSite Developer
*
* licence:
* 		GNU GPLv2
* 		Copyright (C) 2012 ImmortalPC
*
*		This program is free software: you can redistribute it and/or modify
*		it under the terms of the GNU General Public License as published by
*		the Free Software Foundation; either version 2 of the License, or
*		(at your option) any later version.
*
*		This program is distributed in the hope that it will be useful,
*		but WITHOUT ANY WARRANTY; without even the implied warranty of
*		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*		GNU General Public License for more details.
*
*		You should have received a copy of the GNU General Public License
*		along with this program.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
*		Or write to the Free Software
* 		Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Introduction:
* 	This script permit you to auto upload your commit into your ftp.
* 	Warning: ONLY for bitbucket !
* 	Ex:
*		Note: DEST_DIR=dev/
* 		git add test.php
* 		git commit -am "Adding Test"
* 		git pull						<- test.php will be uploaded at http://website.com/dev/test.php
* 		echo "Hello the world" > test.php
* 		git commit -am "Update Test"
* 		git pull						<- test.php will be uploaded at http://website.com/dev/test.php
*
* Created By:
*		ImmortalPC
*
* Last update:
*		17/07/2012
*
* Encoded:
* 		UTF-8
*
* Tabs:
* 		Real Tabs ( NOT SPACE ) with size 4
*
* USE:
* 		1) Change define USER and PASSWORD with your own login for bitbucket
* 		2) Set the DEST_DIR. The DEST_DIR is the directory where your file commited will be uploaded.
* 		Using a dev directory is important ! DO NOT USE the main directory ! Why ? If your commit has some bug, your website will be buggy.
* 		3) Upload this fils onto your server. ( Need to have a public access ! Ex: http://website.com/bitbucket.php )
* 		4) Go to bitbucket into your repo.
* 		5) Go into the admin tab.
* 		6) Go into the subtab "Services"
* 		7) Search in the list: "POST" and add it
* 		8) Now, set the input URL with your script url (ex: http://website.com/bitbucket.php )
*		9) Save. ( Just after URL input )
* 		10) Enjoy.
*/
define('USER', 'ImmortalPC');//				Your USER name for bitbucket login
define('PASSWORD', '88 miles per hour');//	Your PASSWORD for bitbucket login
define('DEST_DIR', 'dev/');// 				Files destination ( Warning DEST_DIR need chmod 0777 )

ini_set('track_errors', true);// On log les erreurs php
ini_set('user_agent', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Ubuntu/10.10 Chromium/16.0.912.77 Chrome/16.0.912.77 Safari/535.7');


/*******************************************************************************
* Anti-Hacker
*/
if( !IsSet($_POST['payload']) ){
	header('HTTP/1.0 404 Not Found');
	header('Status: 404 Not Found');
	echo '<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>404 Not Found</title>
</head><body>
<h1>Not Found</h1>
<p>The requested URL ',$_SERVER['PHP_SELF'],' was not found on this server.</p>
</body></html>';
	exit();
}


/*******************************************************************************
* BitBucket
*/
$info = json_decode($_POST['payload']);
$commits = $info->commits;
define('REPO_URL', 'https://api.bitbucket.org/1.0/repositories'.$info->repository->absolute_url.'raw/master/');

$commits = $info->commits;
$matches = null;
$err = array();
foreach( $commits as $key => $com )
{
	/***************************************************************************
	* Installation des fichiers
	*/
	foreach( $com->files as $fileKey => $fileInfo )
	{
		if( $fileInfo->type === 'removed' ){
			if( is_file(DEST_DIR.$fileInfo->file) ){
				unlink(DEST_DIR.$fileInfo->file);
			}elseif( is_dir(DEST_DIR.$fileInfo->file) ){
				rrmdir(DEST_DIR.$fileInfo->file);
			}
		}else{
			// On regarde si le dossier de destination existe
			$dir = DEST_DIR.$fileInfo->file;
			if( substr($dir, -1) !== '/' ){
				$dir = explode('/', DEST_DIR.$fileInfo->file);
				array_pop($dir);
				$dir = implode('/', $dir).'/';
			}

			// On créer le dossier s'il n'existe pas
			if( !is_dir($dir) )
				mkdir( $dir, 0777, true );

			// Installation du nouveau fichier
			file_put_contents(DEST_DIR.$fileInfo->file, getFile(REPO_URL.$fileInfo->file));
		}
	}
}
// Log des erreurs
if( count($err) && ($fp = fopen(DEST_DIR.'bitBucketError.log', 'a+')) ){
	fputs($fp, var_export($err, true));
	fclose($fp);
	chmod(DEST_DIR.'bitBucketError.log', 0777);// On permet la lecture du log par n'importe qui
}


/*******************************************************************************
* @brief Permet d'obtenir le contenue d'un fichier stocké sur bitbucket
* @param[in] $url	{String} URL complete vers le fichier
* @return {String|Binary} Les données du fichier
*/
function getFile( $url )
{
	global $err;
	$c = curl_init();
	curl_setopt($c, CURLOPT_URL, $url);
	// On indique à curl de nous retourner le contenu de la requête plutôt que de l'afficher
	curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
	// On indique à curl de ne pas retourner les headers http de la réponse dans la chaine de retour
	curl_setopt($c, CURLOPT_HEADER, false);
	// Auth
	curl_setopt($c, CURLOPT_USERPWD, USER.':'.PASSWORD);
	// On execute la requete
	$output = curl_exec($c);
	// On a une erreur alors on la leve
	if( $output === false ){
		array_push($err, curl_error($c));
	}else{
		// On ferme la ressource
		curl_close($c);
		// Si tout c'est bien passé on affiche le contenu de la requête
		return $output;
	}
	// On ferme la ressource
	curl_close($c);
}


/*******************************************************************************
* @brief Permet de supprimer un dossier récurssivement
* @param[in] $dir	{String} Dossier a supprimer
* @return[NONE]
*/
function rrmdir( $dir )
{
	foreach( glob($dir . '/*') as $file )
	{
		if(is_dir($file))
			rrmdir($file);
		else
			unlink($file);
	}
	rmdir($dir);
}

?>
