#!/usr/bin/php
<?php
/*******************************************************************************
* Bitbucket Issues Manager (BIM)
*
* licence:
* 		GNU GPLv2
* 		Copyright (C) 2012 ImmortalPC
*
*		BIM is free software: you can redistribute it and/or modify
*		it under the terms of the GNU General Public License as published by
*		the Free Software Foundation; either version 2 of the License, or
*		(at your option) any later version.
*
*		This program is distributed in the hope that it will be useful,
*		but WITHOUT ANY WARRANTY; without even the implied warranty of
*		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*		GNU General Public License for more details.
*
*		You should have received a copy of the GNU General Public License
*		along with this program.  If not, see <http://www.gnu.org/licenses/gpl-2.0.html>.
*		Or write to the Free Software
* 		Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Introduction:
* 	This script permit you to manage your issues on bitbucket
* 	Warning: ONLY for bitbucket !
* 	Ex:
* 		tk			<- Will show you an interactive listview of issues not closed. Use arrow on the keyboard to navigate. Use Enter to open an issue. Q for exit
* 		new bug "SEGFAULT when get 1.21 gigawatts..."		<- Send a new bug (issue) on bitbucket
* 		new bug		<- With this simple commande, you have multi line mode. You can also add a title
* 		new task "Get 1.21 gigawatts"						<- Send a new task (issue) on bitbucket
*		new task	<- With this simple commande, you have multi line mode. You can also add a title
*
* Created By:
*		ImmortalPC
*
* Last update:
*		17/07/2012
*
* Encoded:
* 		UTF-8
*
* Tabs:
* 		Real Tabs ( NOT SPACE ) with size 4
*
* USE:
* 		1) Change define USER and PASSWORD with your own login for bitbucket
* 		2) Install: sudo ./bitbucket.sh.php install
*		NOTE: Don't forget to enable issues service to get full experience.
* 		1) Go to bitbucket into your repo.
* 		2) Go into the admin tab.
* 		3) Go into the subtab "Services"
* 		4) Search in the list: "Issues" and add it
*		5) Save.
* 		6) Enjoy.
*/
mb_internal_encoding('UTF-8');

define('USER', 'ImmortalPC');//				Your USER name for bitbucket login
define('PASS', '88 miles per hour');//		Your PASSWORD for bitbucket login


// Couleurs
define('COLOR_GREY',	0);
define('COLOR_RED',		1);
define('COLOR_GREEN',	2);
define('COLOR_YELLO',	3);
define('COLOR_BLUE',	4);
define('COLOR_VIOLET',	5);


/*******************************************************************************
* Système d'installation
*/
if( preg_match('`\.(php|sh)$`i', $argv[0]) ){
	if( count($argv) >= 2 && strtolower(trim($argv[1])) === 'install' ){
		echo	color('Installation', COLOR_VIOLET, 1),"\r\n"
				,'	',color('Veillez a lancer l\'installation en mode root.', COLOR_RED),"\r\n"
				,'	',color('Note: Remplissez bien les define: USER et PASS du script ', COLOR_RED),__FILE__,"\r\n"
				,'	L\'utilisateur utilisé pour bitbucket: ',USER,"\r\n"
				,'	Mot de passe utilisé pour bitbucket: ',substr(PASS,0,-5),"*****\r\n"
				,'	Copie du fichier ',__FILE__,' vers /usr/local/bin/bitbucket ';
		if( copy(__FILE__, '/usr/local/bin/bitbucket') ){
			echo '[',color('OK', COLOR_GREEN),"]\r\n";
		}else{
			exit('['.color('FAIL', COLOR_RED)."]\r\n".'Avez vous lancé la commande en root ?'."\r\n");
		}
		echo 'Application du chmod 755 sur /usr/local/bin/bitbucket ';
		if( chmod('/usr/local/bin/bitbucket', 0755) ){
			echo '[',color('OK', COLOR_GREEN),"]\r\n";
		}else{
			echo '[',color('FAIL', COLOR_RED),"]\r\n";
		}
		if( is_file('/usr/local/bin/new') ){
			echo	'	Suppression du VIEUX lien symbolic vers new	';
			if( unlink('/usr/local/bin/new') ){
				echo '[',color('OK', COLOR_GREEN),"]\r\n";
			}else{
				exit('['.color('FAIL', COLOR_RED)."]\r\n".'Avez vous lancé la commande en root ?'."\r\n");
			}
		}
		echo	'	Création du lien symbolic vers new		';
		if( symlink('/usr/local/bin/bitbucket', '/usr/local/bin/new') ){
			echo '[',color('OK', COLOR_GREEN),"]\r\n";
		}else{
			exit('['.color('FAIL', COLOR_RED)."]\r\n".'Avez vous lancé la commande en root ?'."\r\n");
		}
		echo 'Application du chmod 755 sur /usr/local/bin/new ';
		if( chmod('/usr/local/bin/new', 0755) ){
			echo '[',color('OK', COLOR_GREEN),"]\r\n";
		}else{
			echo '[',color('FAIL', COLOR_RED),"]\r\n";
		}
		if( is_file('/usr/local/bin/tk') ){
			echo	'	Suppression du VIEUX lien symbolic vers tk	';
			if( unlink('/usr/local/bin/tk') ){
				echo '[',color('OK', COLOR_GREEN),"]\r\n";
			}else{
				exit('['.color('FAIL', COLOR_RED)."]\r\n".'Avez vous lancé la commande en root ?'."\r\n");
			}
		}
		echo	'	Création du lien symbolic vers tk		';
		if( symlink('/usr/local/bin/bitbucket', '/usr/local/bin/tk') ){
			echo '[',color('OK', COLOR_GREEN),"]\r\n";
		}else{
			exit('['.color('FAIL', COLOR_RED)."]\r\n".'Avez vous lancé la commande en root ?'."\r\n");
		}
		echo 'Application du chmod 755 sur /usr/local/bin/tk ';
		if( chmod('/usr/local/bin/tk', 0755) ){
			echo '[',color('OK', COLOR_GREEN),"]\r\n";
		}else{
			echo '[',color('FAIL', COLOR_RED),"]\r\n";
		}
		exit(
			color('Installation terminée', COLOR_GREEN)."\r\n"
			.'Vous avez maintenant accès aux commandes suivantes:'."\r\n"
			.'	new		Permet de déclarer un nouveau bug ou une nouvelle tache.'."\r\n"
			.'	tk 		Permet de lire les tickets (issues). Utiliser les flèches du clavier pour naviguer. Q pour Quitter.'."\r\n"
			.'	bitbucket	Pour plus d\'info ;-)'."\r\n"
		);
	}
	echo 'Il est préférable d\'installer le script.',"\r\n\r\n";
}


/***************************************************************************//*!
* @brief Permet d'afficher les différentes syntaxes autorisées
* @param[in,opt] $msg		{String} Message d'erreur a afficher
* @return[NONE]
*/
function syntax( $msg = '' )
{
	exit('Bitbucket Issues Manager (BIM) version 2012.07.17 Copyright (C) 2012  ImmortalPC'."\r\n"
    .'BIM comes with ABSOLUTELY NO WARRANTY.'."\r\n"
    .'This is free software, and you are welcome to redistribute it'."\r\n"
    .'under certain conditions. For details, see the GNU General Public License v2.'."\r\n"
    .'<http://www.gnu.org/licenses/gpl-2.0.html>'."\r\n"
    ."\r\n"
	.'Syntaxe:'."\r\n"
	.color('Ajouter un ticket:', COLOR_VIOLET, 1)."\r\n"
	.'    new [trivial|triv|minor|min|major|maj|critical|crit|block] bug|task MSG'."\r\n"
	.'    new [trivial|triv|minor|min|major|maj|critical|crit|block] bug|task'."\r\n"
	.'        > Cette requete permet de spécifier un titre'."\r\n"
	.color('Lire les tickets:', COLOR_VIOLET, 1)."\r\n"
	.'    ticket|tk|issue|issues [trivial|triv|minor|min|major|maj|critical|crit|block] [bug|task] [SEARCH]'."\r\n"
	.'    ticket|tk|issue|issues [rm|remove] id_Ticket'."\r\n"
	.color('Supprimer les tickets fermés:', COLOR_VIOLET, 1)."\r\n"
	.'    ticket|tk|issue|issues clean'."\r\n"
	.color('Projet en cours:', COLOR_VIOLET, 1)."\r\n"
	.'    status|st'."\r\n"
	.color('Aide:', COLOR_VIOLET, 1)."\r\n"
	.'    bitbucket'."\r\n"
	.color($msg, COLOR_RED)."\r\n"
	);
}


if( strtolower(implode(' ', $argv)) === 'bitbucket warranty' ){

	exit('');
}


if( strtolower(implode(' ', $argv)) === 'bitbucket conditions' ){

	exit('');
}

/*******************************************************************************
* On vérifit que la syntax est correct
*/
$matches = '';
if(
	!preg_match('`^[^ ]* ?(new) (trivial|triv|minor|min|major|maj|critical|crit|block)? ?(bug|task)(.*)`i', implode(' ', $argv), $matches )
	&&
	!preg_match('`^[^ ]* ?(ticket|tk|issue|issues) (clean)( ?)( ?)`i', implode(' ', $argv), $matches )
	&&
	!preg_match('`^[^ ]* ?( ?)(status|st)( ?)( ?)`i', implode(' ', $argv), $matches )
	&&
	!preg_match('`^[^ ]* ?(ticket|tk|issue|issues) (status|st)( ?)( ?)`i', implode(' ', $argv), $matches )
	&&
	!preg_match('`^[^ ]* ?(ticket|tk|issue|issues) ?(trivial|triv|minor|min|major|maj|critical|crit|block)? ?(bug|task)? ?(.*)`i', implode(' ', $argv), $matches )
	)
	syntax('Syntax incorrect !'."\r\n");


/*******************************************************************************
* Obtention du projet en cours.
*/
$working_dir_project = realpath('./');
while( !is_file($working_dir_project.'/.git/config') && $working_dir_project !== '/' )
	$working_dir_project = dirname($working_dir_project);

if( $working_dir_project !== '/' ){
	define('GIT_DIR', $working_dir_project);
	$matchDir = null;
	if( preg_match('`(ssh\://)?git@bitbucket\.org(/|:)?([A-Za-z0-9/\.\-_]+)\.git`i', file_get_contents($working_dir_project.'/.git/config'), $matchDir) )
		define('DIR_PROJECT', $matchDir[3]);
	else
		exit(color('No git project here.'."\r\n", COLOR_RED));
}else
	exit(color('No git project here.'."\r\n", COLOR_RED));
unset($working_dir_project, $matchDir);



// New ou ticket
$action = strtolower($matches[1]);

// Priority
$init_priority = strtolower($matches[2]);
switch($init_priority)
{
	default:
	case 'trivial':
	case 'triv':
		$priority = array('code'=>1, 'msg'=>'trivial');// trivial
		break;

	case 'minor':
	case 'min':
		$priority = array('code'=>2, 'msg'=>'minor');
		break;

	case 'major':
	case 'maj':
		$priority = array('code'=>3, 'msg'=>'major');
		break;

	case 'critical':
	case 'crit':
		$priority = array('code'=>4, 'msg'=>'critical');
		break;

	case 'block':
		$priority = array('code'=>5, 'msg'=>'blocker');
		break;
}

// Système de suppression des tickets close
$ticketClean = false;
if( $init_priority === 'clean' )
	$ticketClean = true;

// Système de status
if( in_array($init_priority, array('status','st')) ){
	echo 'User: ',USER,"\r\n"
	,'Current project: ',DIR_PROJECT,"\r\n";
	exit;
}


// Type: bug / task
$kind = strtolower($matches[3]);

// text
$msg = trim($matches[4]);

// Nétoyage
unset($matches);

// Révision local
$local_rev = '???';
$rev = '';
if( defined('GIT_DIR') )
	exec('git log --graph --oneline --all --no-ext-diff -n 1', $rev);

if( IsSet($rev[0]) && preg_match('`[^0-9a-z]+([0-9a-z]+)`i', $rev[0], $matches) )
	$local_rev = $matches[1];

// Nétoyage
unset($matches, $rev);


/*******************************************************************************
* New issue
*/
if( $action === 'new' ){
	$title = (strlen($msg)>=20)?substr($msg, 0, 20).'...':$msg;
	if( empty($msg) ){
		echo 'New ',$priority['msg'],' ',$kind,"\r\n"
		,'Server: ',DIR_PROJECT,"\r\n"
		,'Titre: ';
		$title = trim(fgets(STDIN));
		echo 'Message: (End with ²):',"\r\n:\\> ";
		$msg = '';
		do{
			$tmp = trim(fgets(STDIN));
			$msg .= $tmp."\r\n";
		}while( !preg_match('`²`', $tmp) );
		$msg = trim(str_replace('²', '', $msg));
	}
	echo 'Send new ',$priority['msg'],' ',$kind,' to server with msg "',$title,'" rev ',$local_rev,"\r\n";
		$tmp=sendMsg('', array('post'=>array(
			'title'=>$title,// title: The title of the new issue.
			'content'=>$msg."\r\n\r\n".'Rev Local: '.$local_rev,// content: The content of the new issue.
			// component: The component associated with the issue.
			// milestone: The milestone associated with the issue.
			//version: The version associated with the issue.
			//responsible: The username of the person responsible for the issue.
			'priority'=>$priority['code'],
			'status'=>'new',// status: The status of the issue (new, open, resolved, on hold, invalid, duplicate, or wontfix).
			'kind'=>$kind// kind: The kind of issue (bug, enhancement, or proposal).
		)));

		if( $tmp['code'] == 200 )
			echo color('OK', COLOR_GREEN)."\r\n";
		else
			echo color('Erreur', COLOR_RED)."\r\n".$tmp['output']."\r\n";
	exit();
}


/*******************************************************************************
* View issue
*/
// else $action === 'ticket|tk|issue|issues'
if( is_numeric($msg) ){
	$val = sendMsg($msg.'/');
	if( $val['code'] != 200 )
		exit(color('Erreur', COLOR_RED)."\r\n".$val['output']."\r\n");
	$val = json_decode($val['output']);
	if( !$val )
		exit('Ticket introuvable'."\r\n");

viewTicket:// Cf goto plus bas
	// Affichage
	printf("\033[H\033[2J");// Clear Screen
	printf("\033[32;1m\033[40m[%-5s][%-40s] [Status   ] [Priority] [%-20s]\033[0m\r\n\033[40m", 'id', 'Titre', 'Date');
	printf('%+6s  ', $val->local_id);
	printf('%-'.(40+getNbChar($val->title, 'éèàçêï')).'s  ', substr($val->title,0, 40));
	echo "\033[40m";// BG black
	if( $val->status === 'new' )
		printf("\033[32;1m[%-9s]\033[0m", $val->status);// Vert claire
	else
		printf('[%-9s]', $val->status);

	echo "\033[40m ";// BG black
	if( $val->priority === 'major' )
		printf("\033[31;1m[%-8s]\033[0m", $val->priority);
	elseif( $val->priority === 'critical' )
		printf("\033[7m\033[31;1m[%-8s]\033[0m", $val->priority);
	else
		printf("[%-8s]", $val->priority);

	printf("\033[40m".'  %-20s '."\033[0m\r\n", $val->created_on);
	echo "\033[40m",str_repeat('=', 95),"\033[0m\r\n";
	echo $val->content,"\r\n";

	if( is_file('/bin/stty') ){
		$key = '';
		while( true )
		{
			echo "\033[4m",'Prendre le ticket ? (Oui, Non, Yes, No, Escap, Quit, Supprimer)',"\033[0m\r\n";
			$key = dynamiqInput();
			/*
			* 27	Echap
			* 121	y
			* 89	Y
			* 111	o
			* 79	O
			* 110	n
			* 78	N
			* 113	q
			* 81	Q
			* 115	s
			* 83	S
			*/
			// Quitter
			if( in_array($key, array(27, 110, 78, 113, 81)) )
				if( IsSet($highlight) )
					goto retunToList;
				else
					exit("\r\n");

			// Suppression
			if( in_array($key, array(115, 83)) ){
				echo "\033[31;1mÊtes vous sûr ? (Oui|Non)\033[0m\r\n:\\> ";
				if( strtolower(trim(fgets(STDIN))) === 'o' )
					sendMsg($val->local_id.'/', array('delete'=>true));
				else
					echo 'Annulé.',"\r\n";
				exit();
			}

			// Ouverture du ticket
			if( in_array($key, array(121, 89, 111, 79)) ){
				sendMsg($val->local_id.'/', array('put'=>array(
					'status'=>'open'// status: The status of the issue (new, open, resolved, on hold, invalid, duplicate, or wontfix).
				)));
				break;
			}

			echo 'Actions possibles: Oui, Non, Yes, No, Quit, Escap',"\r\n";
		}

	}else{
		$quit = false;
		do{
			echo "\033[4m",'Prendre le ticket ?',"\033[0m\r\n\033[40m:\\> \033[0m";
			stream_set_blocking($stdin, false);
			$input = strtolower(trim(fgets(STDIN)));

			if( in_array($input, array('n','q')) )
				exit("\r\n");

			if( in_array($input, array('o','y')) )
				break;

			echo 'Actions possibles: Oui, Non, Yes, No',"\r\n";
		}while( !in_array($input, array('n','q')) );
	}

	if( $val->status !== 'new' )
		exit('Le ticket ne peut être pris.'."\r\n");

	echo 'Prise du ticket',"\r\n";

	exit;
}


/*******************************************************************************
* REMOVE in issues
*/
if( $ticketClean === true ){//\033[31;1m[%-8s]\033[0m
	echo "Voulez vous vraiment \033[31;1m\033[4msupprimer\033[0m \033[4mtout\033[0m les tickets fermés ?(Oui|Non)\r\n:\\> ";
	if( strtolower(trim(fgets(STDIN))) === 'o' ){
		$ret = json_decode(sendMsg('?status=resolved'));
		if( $ret['code'] == 200 ){
			foreach( $ret->issues as $key => $val )
				sendMsg($val->local_id.'/', array('delete'=>true));
			echo count($ret->issues),' tickets ont été supprimé.',"\r\n";
		}else{
			echo "\033[31;1mErreur\033[0m\r\n",$ret['output'],"\r\n";
		}
	}else{
		echo 'Action annulée.',"\r\n";
	}
	exit();
}


/*******************************************************************************
* SEARCH & LIST in issues
*/
$offsetStart = 0;
$offsetLimit = 20;
reloadTicket:
$req = '?';

// Status
$req .= 'status=new&status=open&start='.$offsetStart.'&limit='.$offsetLimit;

// Kind
if( !empty($kind) )
	$req .= '&kind='.$kind;

// Priority
if( !empty($init_priority) )
	$req .= '&priority='.urlencode($priority['msg']);

// Content
if( !empty($msg) ){
	if( !preg_match('`&|=`', $msg) )
		$msg = urlencode($msg);
	$req .= (substr($msg, 0, 1)==='&'?'':'&q=').$msg;
}

$ret = sendMsg($req);
if( $ret['code'] != 200 )
	exit("\033[31;1mErreur\033[0m\r\n".$ret['output']."\r\n");
$ret = json_decode($ret['output']);
if( !$ret )
	exit('Service indisponnible'."\r\n");

if( !count($ret->issues) )
	exit("Pas de ticket\r\n");

// Affichage FIX
if( !is_file('/bin/stty') ){
	printf("\033[32;1m\033[40m[%-5s][%-40s] [Status   ] [Priority] [%-20s]\033[0m"."\r\n", 'id', 'Titre', 'Date');
	foreach( $ret->issues as $key => $val )
	{
		printf('%+6s  ', $val->local_id);
		printf('%-40s  ', substr($val->title,0, 40));
		if( $val->status === 'new' )
			printf("\033[32;1m[%-9s]\033[0m ", $val->status);// Vert claire
		else
			printf('[%-9s] ', $val->status);

		if( $val->priority === 'major' )
			printf("\033[31;1m[%-8s]\033[0m", $val->priority);
		elseif( $val->priority === 'critical' )
			printf("\033[7m\033[31;1m[%-8s]\033[0m", $val->priority);
		else
			printf("[%-8s]", $val->priority);

		printf('  %-20s'."\r\n", $val->created_on);
	}
	exit;
}

// Affichage interactif
$input = '';
$highlight = 0;
$upDown = false;
retunToList:
do{

	do{
		unset($ListOfId);
		$ListOfId = Array();
		printf("\033[H\033[2J");// Clear Screen
		printf("\033[32;1m\033[40m[%-5s][%-40s] [Status   ] [Priority] [%-20s]\033[0m"."\r\n", 'id', 'Titre', 'Date');
		$i = 0;
		foreach( $ret->issues as $key => $val )
		{
			if( !empty($input) && !preg_match('`^'.$input.'`', $val->local_id) && !preg_match('`^'.$input.'`i', $val->title) )
				continue;

			// Anti-bug de filtre
			array_push($ListOfId, $key);

			if( $highlight === $i ){
				printf("\033[7m%+6s  ", $val->local_id);
				printf('%-'.(40+getNbChar($val->title, 'éèàçêï')).'s  ', trim(substr($val->title,0, 40)));
				if( $val->status === 'new' )
					printf("\033[32;1m[%-9s]\033[0m\033[7m ", $val->status);// Vert claire
				else
					printf('[%-9s] ', $val->status);

				echo "\033[7m";
				if( $val->priority === 'major' )
					printf("\033[31;1m[%-8s]\033[0m", $val->priority);
				elseif( $val->priority === 'critical' )
					printf("\033[7m\033[31;1m[%-8s]\033[0m", $val->priority);
				else
					printf("[%-8s]\033[0m", $val->priority);

				printf("\033[7m  %-20s\033[0m\r\n", $val->created_on);


			}else{
				printf('%+6s  ', $val->local_id);
				printf('%-'.(40+getNbChar($val->title, 'éèàçêï')).'s  ', trim(substr($val->title,0, 40)));
				if( $val->status === 'new' )
					printf("\033[32;1m[%-9s]\033[0m ", $val->status);// Vert claire
				else
					printf("[%-9s]\033[0m ", $val->status);

				if( $val->priority === 'major' )
					printf("\033[31;1m[%-8s]\033[0m", $val->priority);
				elseif( $val->priority === 'critical' )
					printf("\033[7m\033[31;1m[%-8s]\033[0m", $val->priority);
				else
					printf('[%-8s]', $val->priority);

				printf("  %-20s\033[0m\r\n", $val->created_on);
			}
			$i++;
		}
		if( $ret->count > $offsetLimit && count($ret->issues) === $offsetLimit )
			printf("  %+91s\r\n", '-->');

		if( !empty($input) )
			echo 'Filtre: ',$input,"\r\n";
		$key = dynamiqInput();

		if( $key == 27 ){
			// Données inutiles
			if( dynamiqInput() == 27 )
				exit("\r\n");
			$key = dynamiqInput();

			$upDown = true;
		}

		if( $upDown ){
			if( $key == 66 ){// Bas
				$highlight++;
				if( $highlight >= count($ret->issues) )
					$highlight = 0;

			}else if( $key == 65 ){// Haut
				$highlight--;
				if( $highlight < 0 )
					$highlight = count($ret->issues)-1;

			}else if( $key == 67 ){// Droite
				if( count($ret->issues) === $offsetLimit ){
					echo 'Chargement...',"\r\n";
					$offsetStart += $offsetLimit;
					goto reloadTicket;
				}

			}else if( $key == 68 ){// Gauche
				if( ($offsetStart -= $offsetLimit) < 0 )
					$offsetStart = 0;
				else{
					echo 'Chargement...',"\r\n";
					goto reloadTicket;
				}
			}
		}else{
			if( (ord('0') <= $key && $key <= ord('9')) || (ord('a') <= $key && $key <= ord('z')) || (ord('A') <= $key && $key <= ord('Z')) || $key == ord(' ') )
				$input .= chr($key);

			// Touche espace
			if( $key == 0 )
				$input .= ' ';

			// Retour en arrière
			if( $key == 127 )
				$input = trim(substr($input, 0, strlen($input)-1));

			if( $key == 13 ){// Entrer
				$val = $ret->issues[$ListOfId[$highlight]];
				goto viewTicket;
			}
		}

		$upDown = false;
	}while( !in_array($key, array(27, 113, 81)) );
	exit("\n");
	$key = dynamiqInput();
	echo $key;

}while( true );















/***************************************************************************//*!
* @brief Permet d'envoyer une requete au serveur bitBucket
* @param[in,opt] $get		{String} URL (Paramètres) a envoyer au serveurs
* @param[in,opt] $arg		{Array} Données a envoyer. $arg['post'] = array(), $arg['put'] = array(), $arg['delete'] = array()
* @return Array('code'=>int, 'output'=>JSON)
*/
function sendMsg( $get='', $arg='' )
{
	$c = curl_init();
	curl_setopt($c, CURLOPT_URL, 'https://api.bitbucket.org/1.0/repositories/'.DIR_PROJECT.'/issues/'.$get);
	// On indique à curl de nous retourner le contenu de la requête plutôt que de l'afficher
	curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
	// On indique à curl de ne pas retourner les headers http de la réponse dans la chaine de retour
	curl_setopt($c, CURLOPT_HEADER, false);
	// On indique à curl d'envoyer une requete post
	if( IsSet($arg['post']) ){
		curl_setopt($c, CURLOPT_POST, true);
		// On donne les paramêtre de la requete post
		curl_setopt($c, CURLOPT_POSTFIELDS, $arg['post']);
	}
	// Methode PUT
	if( IsSet($arg['put']) ){
		$putString = utf8_encode( http_build_query( $arg['put'], '', '&' ) );
		$putData = tmpfile();
		fwrite($putData, $putString);
		fseek($putData, 0);

		curl_setopt($c, CURLOPT_PUT, true);
		curl_setopt($c, CURLOPT_INFILE, $putData);
		// On donne les paramêtre de la requete post
		curl_setopt($c, CURLOPT_INFILESIZE, strlen($putString));
	}
	// Methode DELETE
	if( IsSet($arg['delete']) )
		curl_setopt($c, CURLOPT_CUSTOMREQUEST, 'DELETE');

	// Auth
	curl_setopt($c, CURLOPT_USERPWD, USER.':'.PASS);

	// On execute la requete
	$return = array('output'=>curl_exec($c));
	$return['code'] = curl_getinfo($c, CURLINFO_HTTP_CODE);

	// On a une erreur alors on la leve
	if( $return['output'] === false )
		exit(curl_error($c));

	// On ferme la ressource
	curl_close($c);
	return $return;
}


/***************************************************************************//*!
* @brief Permet d'avoir un accès au clavier de façon non blockante
* @return KeyCode
*/
function dynamiqInput()
{
	$key = null;
	/*
	exec('stty -echo -icanon time 0 min 0; while :;'
	.'do '
		.'key=$( dd bs=1 count=1 2> /dev/null; printf "." );'
		.'key=${key%.};'
		.'case $key in '
			.'?) printf "%d\n" "\'$key";'
			.'break;;'
		.'esac;'
	.'done;'
	.'stty sane;', $key);
	*/
	exec('stty_state=`stty -g`;'
	.'stty raw; stty -echo;'
	.'keycode=`dd bs=1 count=1 2>/dev/null`;'
	.'stty "$stty_state"; printf %d \\\'$keycode;', $key);

	return $key[0];
}


/***************************************************************************//*!
* @brief Permet de coloriser le texte {$txt} avec la couleur {$color}.
* De plus, le texte peut être souligné {$underline}
* @param[in] $txt			{String} Le texte a coloriser
* @param[in] $color			{int} Couleur du texte. VOIR les define commencant par COLOR_
* @param[in,opt] $underline	{bool} Activer le soulignement ?
* @return Le texte colorisé.
*
* @warning Cette fonction utilise les caractères d'échapement des consoles UNIX pour coloriser le texte.
* Cela implique donc que ça ne marchera pas sous Windows.
*/
function color( $txt, $color, $underline=false )
{
	if( PHP_SHLIB_SUFFIX === 'dll' )// Si l'OS est windows => pas d'affichage colorisé
		return $txt;

	if( $underline )
		return "\033[3".$color.";4m"."\033[3".$color.";1m".$txt."\033[0m";
	return "\033[3".$color.";1m".$txt."\033[0m";
}


/***************************************************************************//*!
* @brief Permet de compter le nombre de caractère dans un texte.
* @param[in] $text			{String} Le texte a analyser
* @param[in] $charList		{String} Liste des caractères a chercher
* @return Le nombre de caractères trouvé dans $text et qui sont dans la liste $charList
*
* @code
* echo getNbChar( 'élo', 'éèï' );// return 1
* echo getNbChar( 'élèo', 'éèï' );// return 2
* echo getNbChar( 'élo', 'éèïl' );// return 2
* echo getNbChar( 'hello', 'éèï' );// return 0
* @endcode
*/
function getNbChar( $text, $charList )
{
	$nb=0;
	for( $i=0,$size=mb_strlen($charList); $i<$size; $i++ )
		$nb += mb_substr_count($text, mb_substr( $charList, $i, 1 ));
	return $nb;
}

?>
